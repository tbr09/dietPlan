﻿using System.Web.Mvc;
using DietPlan.Logic.Abstraction;

namespace DietPlan.Web.Controllers
{
    public class HomeController : Controller
    {
        private ICategoryManager categoryManager;

        public HomeController(ICategoryManager categoryManager)
        {
            this.categoryManager = categoryManager;
        }
        public ActionResult Index()
        {
            var res = categoryManager.GetAll();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}