﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DietPlan.Web.Startup))]
namespace DietPlan.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
