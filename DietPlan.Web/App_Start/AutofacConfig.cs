﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DietPlan.Logic.Abstraction;
using DietPlan.Logic.Implementation;
using DietPlan.Repository.DataAccess;
using DietPlan.Repository.DataConnection;

namespace DietPlan.Web
{
    public class AutofacConfig
    {
        public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //// OPTIONAL: Register model binders that require DI.
            //builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            //builder.RegisterModelBinderProvider();

            //// OPTIONAL: Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();

            //// OPTIONAL: Enable property injection in view pages.
            //builder.RegisterSource(new ViewRegistrationSource());

            //// OPTIONAL: Enable property injection into action filters.
            //builder.RegisterFilterProvider();

            builder.RegisterType(typeof(DietPlanContext));

            builder.RegisterType(typeof(UnitOfWork))
                .As(typeof(IUnitOfWork));

            builder.RegisterType(typeof(CategoryManager))
                .As(typeof(ICategoryManager));

            builder.RegisterType(typeof(DailyPlanManager))
                .As(typeof(IDailyPlanManager));

            builder.RegisterType(typeof(DietManager))
                .As(typeof(IDietManager));

            builder.RegisterType(typeof(MealManager))
                .As(typeof(IMealManager));

            builder.RegisterType(typeof(ProductManager))
                .As(typeof(IProductManager));


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            
        }
    }
}