﻿using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class DietManager : Manager<Diet>, IDietManager
    {
        public DietManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Diet GetByName(string name)
        {
            var diet = UnitOfWork.Repository<Diet>().Get((x) => x.Name == name);
            return diet;
        }
    }
}
