﻿using System.Collections.Generic;
using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class Manager<T> : IManager<T> where T : class
    {
        protected readonly IUnitOfWork UnitOfWork;


        public Manager(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public void Add(T entity)
        {
            UnitOfWork.Repository<T>().Add(entity);
            UnitOfWork.Commit();
        }

        public void Update(T entity)
        {
            UnitOfWork.Repository<T>().Update(entity);
            UnitOfWork.Commit();
        }

        public void Delete(int entityId)
        {
            UnitOfWork.Repository<Category>().Delete(entityId);
            UnitOfWork.Commit();
        }

        public T GetById(int entityId)
        {
            var entity = UnitOfWork.Repository<T>().GetById(entityId);
            return entity;
        }

        public IEnumerable<T> GetAll()
        {
            var entities = UnitOfWork.Repository<T>().GetAll();
            return entities;
        }
    }
}
