﻿using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class MealManager : Manager<Meal>, IMealManager
    {
        public MealManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Meal GetByName(string name)
        {
            var meal = UnitOfWork.Repository<Meal>().Get((x) => x.Name == name);
            return meal;
        }
    }
}
