﻿using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class CategoryManager : Manager<Category>, ICategoryManager
    {
        public CategoryManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Category GetByName(string name)
        {
            var category = UnitOfWork.Repository<Category>().Get((x) => x.Name == name);
            return category;
        }
    }
}
