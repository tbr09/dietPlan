﻿using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class ProductManager : Manager<Product>, IProductManager
    {
        public ProductManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Product GetByName(string name)
        {
            var product = UnitOfWork.Repository<Product>().Get((x) => x.Name == name);
            return product;
        }
    }
}
