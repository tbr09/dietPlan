﻿using DietPlan.Logic.Abstraction;
using DietPlan.Model;
using DietPlan.Repository.DataAccess;

namespace DietPlan.Logic.Implementation
{
    public class DailyPlanManager : Manager<DailyPlan>, IDailyPlanManager
    {
        public DailyPlanManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public DailyPlan GetByName(string name)
        {
            var dailyPlan = UnitOfWork.Repository<DailyPlan>().Get((x) => x.Name == name);
            return dailyPlan;
        }
    }
}
