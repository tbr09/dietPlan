﻿using DietPlan.Model;

namespace DietPlan.Logic.Abstraction
{
    public interface IDailyPlanManager : IManager<DailyPlan>
    {
        DailyPlan GetByName(string name);
    }
}
