﻿using DietPlan.Model;

namespace DietPlan.Logic.Abstraction
{
    public interface IMealManager : IManager<Meal>
    {
        Meal GetByName(string name);
    }
}
