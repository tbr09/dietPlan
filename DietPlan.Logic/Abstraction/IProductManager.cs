﻿using DietPlan.Model;

namespace DietPlan.Logic.Abstraction
{
    public interface IProductManager : IManager<Product>
    {
        Product GetByName(string name);
    }
}
