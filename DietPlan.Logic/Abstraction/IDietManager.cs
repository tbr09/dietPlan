﻿using DietPlan.Model;

namespace DietPlan.Logic.Abstraction
{
    public interface IDietManager : IManager<Diet>
    {
        Diet GetByName(string name);
    }
}
