﻿using System.Collections.Generic;

namespace DietPlan.Logic.Abstraction
{
    public interface IManager<T> where T : class
    {
        void Add(T entity);

        void Update(T entity);

        void Delete(int entityId);

        T GetById(int entityId);

        IEnumerable<T> GetAll();
    }
}
