﻿using DietPlan.Model;

namespace DietPlan.Logic.Abstraction
{
    public interface ICategoryManager : IManager<Category>
    {
        Category GetByName(string name);
    }
}
