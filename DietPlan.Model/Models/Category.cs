﻿using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public virtual Product Product{ get; set; }
    }
}
