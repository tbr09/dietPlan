﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class Diet
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }

        [Required]
        public bool IsCyclic { get; set; }

        [StringLength(250)]
        public string Description { get; set; }
        
       public virtual ICollection<DailyPlan> DailyPlans { get; set; } = new HashSet<DailyPlan>();
    }
}
