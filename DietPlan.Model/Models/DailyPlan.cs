﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class DailyPlan
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Required]
        public System.DateTime Date { get; set; }


        public virtual ICollection<Meal> DailyPlanMeals { get; set; } = new HashSet<Meal>();

        public virtual Diet Diet { get; set; }
    }
}
