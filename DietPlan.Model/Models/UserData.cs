﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class UserData
    {
        [StringLength(30)]
        public string FirstName { get; set; }

        [StringLength(60)]
        public string Surname { get; set; }

        public float Weight { get; set; }

        public float Height { get; set; }

        public DateTime? BirthDate { get; set; }

        [DataType(DataType.ImageUrl)]
        public string AvatarPath { get; set; }
    }
}
