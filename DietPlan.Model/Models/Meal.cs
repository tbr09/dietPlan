﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class Meal
    {
        public int Id { get; set; }

        [Required]
        public DateTime Hour { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        
        public virtual ICollection<Product> Products { get; set; } = new HashSet<Product>();
    }
}
