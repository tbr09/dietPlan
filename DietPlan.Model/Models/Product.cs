﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DietPlan.Model
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Required]
        public double ApproxPrice { get; set; }

        [Required]
        public double Protein { get; set; }

        [Required]
        public double Carbohydrates { get; set; }

        [Required]
        public double Fat { get; set; }

        [Required]
        public bool IsApproved { get; set; }

        public virtual Meal Meal { get; set; }

        public virtual ICollection<Category> ProductCategories { get; set; } = new HashSet<Category>();
    }
}
