﻿namespace DietPlan.Repository.DataAccess
{
    public interface IUnitOfWork
    {
        IRepository<T> Repository<T>() where T : class;

        void Commit();
    }
}
