﻿using System;
using System.Collections.Generic;
using System.Linq;
using DietPlan.Repository.DataConnection;

namespace DietPlan.Repository.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private DietPlanContext context;
        Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public UnitOfWork(DietPlanContext context)
        {
            this.context = context;
        }
        
        public IRepository<T> Repository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)))
            {
                return repositories[typeof(T)] as IRepository<T>;
            }
            else
            {
                IRepository<T> repo = new RepositoryBase<T>(context);
                repositories.Add(typeof(T), repo);
                return repo;
            }
        }

        public void Commit()
        {
            context.SaveChanges();
        }
    }
}
