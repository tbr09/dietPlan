﻿using System.Data.Entity;
using DietPlan.Model;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DietPlan.Repository.DataConnection
{
    public class DietPlanContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Meal> Meals { get; set; }

        public DbSet<Diet> Diets { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<DailyPlan> DailyPlans { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DietPlanContext() : base("DietPlanContext")
        {
                
        }

        static DietPlanContext()
        {
            Database.SetInitializer(new DietPlanInitializer());
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //}

        public static DietPlanContext Create()
        {
            return new DietPlanContext();
        }
    }
}
