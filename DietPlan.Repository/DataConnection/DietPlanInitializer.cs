﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using DietPlan.Model;
using DietPlan.Repository.Migrations;

namespace DietPlan.Repository.DataConnection
{
    public class DietPlanInitializer : MigrateDatabaseToLatestVersion<DietPlanContext, Configuration>
    {
        public static void SeedDietPlanData(DietPlanContext context)
        {
            var categories = new List<Category>
            {
                new Category() { Name="Meat" },
                new Category() { Name="Vegetables" },
                new Category() { Name="Fish" },
                new Category() { Name="Fruits" },
                new Category() { Name="Milk" },
            };

            categories.ForEach(k => context.Categories.AddOrUpdate(k));

            var products = new List<Product>
            {
                new Product()
                {
                    Name = "Egg", ApproxPrice = 5.0, Protein = 12,Carbohydrates = 11, Fat = 8, IsApproved = true, ProductCategories = new List<Category>(){ categories[1], categories[4] }
                },
                new Product()
                {
                    Name = "Chicken", ApproxPrice = 15.0, Protein = 11,Carbohydrates = 12, Fat = 8, IsApproved = true, ProductCategories = new List<Category>(){ categories[4] }
                },
                new Product()
                {
                    Name = "Beed", ApproxPrice = 11.0, Protein = 7,Carbohydrates = 11, Fat = 8, IsApproved = true, ProductCategories = new List<Category>(){ categories[1] }
                },
                new Product()
                {
                    Name = "Flour", ApproxPrice = 5.0, Protein = 5,Carbohydrates = 5, Fat = 11, IsApproved = true, ProductCategories = new List<Category>(){ categories[3], categories[4] }
                },
                new Product()
                {
                    Name = "Milk", ApproxPrice = 2.0, Protein = 5,Carbohydrates = 5, Fat = 5, IsApproved = true, ProductCategories = new List<Category>(){categories[2] }
                },
                new Product()
                {
                    Name = "Potatoe", ApproxPrice = 5.0, Protein = 5,Carbohydrates = 4, Fat = 11, IsApproved = true, ProductCategories = new List<Category>(){ categories[1], categories[4] }
                },
            };

            products.ForEach(k => context.Products.AddOrUpdate(k));

            var meals = new List<Meal>
            {
                new Meal() {Name = "Red soup", Hour=DateTime.Now, Products = new List<Product>() { products[0], products[2] }},
                new Meal() {Name = "Beef with potatoes", Hour=DateTime.Now, Products = new List<Product>(){ products[4], products[2] }},
                new Meal() {Name = "Mushroom soup", Hour=DateTime.Now, Products = new List<Product>(){ products[2], products[1] }},
                new Meal() {Name = "Gueckamole", Hour=DateTime.Now, Products = new List<Product>(){ products[3], products[1] }},
            };

            meals.ForEach(k => context.Meals.AddOrUpdate(k));

            var dailyPlans = new List<DailyPlan>
            {
                new DailyPlan() { Name="HighProtein Day", Date = DateTime.Now, DailyPlanMeals = new List<Meal>() { meals[0], meals[3]}},
                new DailyPlan() { Name="Vegetable Day", Date = DateTime.Now, DailyPlanMeals = new List<Meal>() { meals[2], meals[1]}},
                new DailyPlan() { Name="Chicken Day", Date = DateTime.Now, DailyPlanMeals = new List<Meal>() { meals[3], meals[2]}},
                new DailyPlan() { Name="HighCalories Dayy", Date = DateTime.Now, DailyPlanMeals = new List<Meal>() { meals[1], meals[2]}},
                new DailyPlan() { Name="HighProtein Day II", Date = DateTime.Now, DailyPlanMeals = new List<Meal>() { meals[2], meals[3]}},
            };

            dailyPlans.ForEach(k => context.DailyPlans.AddOrUpdate(k));

            var diets = new List<Diet>
            {
                new Diet()
                {
                    Name = "HighProtein Diet",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    IsCyclic = false,
                    Description = "Some description about HighProtein Diet",
                    DailyPlans = new List<DailyPlan>() {dailyPlans[0], dailyPlans[1], dailyPlans[2]}
                },
                new Diet()
                {
                    Name = "HighCalories Diet",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    IsCyclic = false,
                    Description = "Some description about HighProtein Diet",
                    DailyPlans = new List<DailyPlan>() {dailyPlans[1], dailyPlans[2]}
                },
                new Diet()
                {
                    Name = "Vegan Diet",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    IsCyclic = false,
                    Description = "Some description about HighProtein Diet",
                    DailyPlans = new List<DailyPlan>() {dailyPlans[1], dailyPlans[2], dailyPlans[4]}
                },
                new Diet()
                {
                    Name = "Vegetarian Diet",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    IsCyclic = false,
                    Description = "Some description about HighProtein Diet",
                    DailyPlans = new List<DailyPlan>() {dailyPlans[3], dailyPlans[1], dailyPlans[4]}
                }
            };

            diets.ForEach(k=>context.Diets.AddOrUpdate(k));
            context.SaveChanges();

            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            //const string name = "admin@shop.pl";
            //const string password = "pass";
            //const string roleName = "Admin";

            //var user = userManager.FindByName(name);
            //if (user == null)
            //{
            //    user = new ApplicationUser { UserName = name, Email = name, userData = new UserData() };
            //    var result = userManager.Create(user, password);
            //}

            //var role = roleManager.FindByName(roleName);
            //if (role == null)
            //{
            //    role = new IdentityRole(roleName);
            //    var roleResult = roleManager.Create(role);
            //}

            //var rolesForUser = userManager.GetRoles(user.Id);
            //if (!rolesForUser.Contains(role.Name))
            //{
            //    var result = userManager.AddToRole(user.Id, role.Name);
            //}
        }
    }
}
