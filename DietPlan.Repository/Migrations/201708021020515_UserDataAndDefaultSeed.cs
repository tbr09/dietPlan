namespace DietPlan.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserDataAndDefaultSeed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "userData_FirstName", c => c.String(maxLength: 30));
            AddColumn("dbo.AspNetUsers", "userData_Surname", c => c.String(maxLength: 60));
            AddColumn("dbo.AspNetUsers", "userData_Weight", c => c.Single(nullable: false));
            AddColumn("dbo.AspNetUsers", "userData_Height", c => c.Single(nullable: false));
            AddColumn("dbo.AspNetUsers", "userData_BirthDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "userData_AvatarPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "userData_AvatarPath");
            DropColumn("dbo.AspNetUsers", "userData_BirthDate");
            DropColumn("dbo.AspNetUsers", "userData_Height");
            DropColumn("dbo.AspNetUsers", "userData_Weight");
            DropColumn("dbo.AspNetUsers", "userData_Surname");
            DropColumn("dbo.AspNetUsers", "userData_FirstName");
        }
    }
}
