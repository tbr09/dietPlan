namespace DietPlan.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitializeDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ApproxPrice = c.Double(nullable: false),
                        Protein = c.Double(nullable: false),
                        Carbohydrates = c.Double(nullable: false),
                        Fat = c.Double(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        Meal_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Meals", t => t.Meal_Id)
                .Index(t => t.Meal_Id);
            
            CreateTable(
                "dbo.Meals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Hour = c.DateTime(nullable: false),
                        Name = c.String(),
                        DailyPlan_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DailyPlans", t => t.DailyPlan_Id)
                .Index(t => t.DailyPlan_Id);
            
            CreateTable(
                "dbo.DailyPlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Date = c.DateTime(nullable: false),
                        Diet_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Diets", t => t.Diet_Id)
                .Index(t => t.Diet_Id);
            
            CreateTable(
                "dbo.Diets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        IsCyclic = c.Boolean(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyPlans", "Diet_Id", "dbo.Diets");
            DropForeignKey("dbo.Meals", "DailyPlan_Id", "dbo.DailyPlans");
            DropForeignKey("dbo.Categories", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.Products", "Meal_Id", "dbo.Meals");
            DropIndex("dbo.DailyPlans", new[] { "Diet_Id" });
            DropIndex("dbo.Meals", new[] { "DailyPlan_Id" });
            DropIndex("dbo.Products", new[] { "Meal_Id" });
            DropIndex("dbo.Categories", new[] { "Product_Id" });
            DropTable("dbo.Diets");
            DropTable("dbo.DailyPlans");
            DropTable("dbo.Meals");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
