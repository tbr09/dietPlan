namespace DietPlan.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetupValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Meals", "Name", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.DailyPlans", "Name", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Diets", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Diets", "Description", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Diets", "Description", c => c.String());
            AlterColumn("dbo.Diets", "Name", c => c.String());
            AlterColumn("dbo.DailyPlans", "Name", c => c.String());
            AlterColumn("dbo.Meals", "Name", c => c.String());
            AlterColumn("dbo.Products", "Name", c => c.String());
            AlterColumn("dbo.Categories", "Name", c => c.String());
        }
    }
}
