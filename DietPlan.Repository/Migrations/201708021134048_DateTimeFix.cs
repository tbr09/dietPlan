namespace DietPlan.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeFix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "userData_BirthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "userData_BirthDate", c => c.DateTime(nullable: false));
        }
    }
}
